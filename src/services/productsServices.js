const _base_url = 'https://bouquets.herokuapp.com/bouquets' 
const _getProducts =  _ => {
    return fetch(_base_url)
        .then(response => response.json())
        .then(response => response)
        .catch(error => {
                alert('Ha ocurrido un error intentando ');
                return [];
            }
        )
}
const _createProduct = async form => {
    if (!form.checkValidity()){
        alert('Debe rellenar todos los campos');
        return { id:0 }
        //throw new Error('MISSING_FIELDS');
    }
    let payload = {};
    payload['name'] = form.name.value;
    payload['price'] = "€" + Number(form.price.value).toFixed(2);
    payload['image'] = form.image.value;

    return await fetch(_base_url, {
        "method": "POST",
        "headers": {
            "Content-Type": "application/json"
        },
        "body": JSON.stringify(payload) 
        })
        .then(response => response.json())
        .then(response => {
            alert('Producto registrado correctamente');
            return response
        })
        .catch(err => {
            alert('Hubo un error intentando registrar el producto');
            return { id: 0}
        });
}
const _deleteProduct = async productId => {

    return await fetch(`${_base_url}/${productId}`, {
        "method": "DELETE",
        "headers": {
            "Content-Type": "application/json"
        }
    })
        .then(response => response)
}
const _getProductInfo = async productId => {
    return await fetch(`${_base_url}/${productId}`)
        .then(response => response.json())
        .then(response => response)
        .catch(error => alert('Hubo un error obteniendo la información'));
}
const _updateProduct = async (productId, form) => {

    if (!form.checkValidity()){
        alert('Debe rellenar todos los campos');
        return { id:0 }
        //throw new Error('MISSING_FIELDS');
    }
    let payload = {};
    payload['name'] = form.name.value;
    payload['price'] = "€" + Number(form.price.value).toFixed(2);
    payload['image'] = form.image.value;
    return await fetch(`${_base_url}/${productId}`, {
        "method": "PUT",
        "headers": {
            "Content-Type": "application/json"
        },
        "body": JSON.stringify(payload) 
        })
        .then(response => response.json())
        .then(response => {
            alert('Producto actualizado correctamente');
            console.log(response)
            return response
        })
        .catch(err => {
            alert('Hubo un error intentando actualizar el producto');
            return { id: 0}
        });
}
module.exports = {
    getProducts : _getProducts,
    createProduct : _createProduct,
    deleteProduct : _deleteProduct,
    getProductInfo : _getProductInfo,
    updateProduct : _updateProduct
}