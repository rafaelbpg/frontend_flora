import React, { useEffect, useState } from 'react';
import productsServices from '../services/productsServices';
import ProductCard from '../components/ProductCard';
import { Link } from 'react-router-dom';

function ProductGrid() {
  const [productList, setProductList] = useState([])
  useEffect( () => {
    if (typeof localStorage.favoriteProducts == 'undefined'){
      localStorage.setItem('favoriteProducts', '{}');
    }
  }, [])
  
  useEffect( async () =>  setProductList( await productsServices.getProducts()), []);
  return (
    <React.Fragment>
      <div className="row justify-content-center">
        <div className="col-12  mt-5">
          <Link to={{pathname:'products/create'}}className='btn btn-dark'>
            Crear nuevo producto
          </Link>
        </div>
        {productList.map(product => <ProductCard 
          key={product.id}
          product={product}
        />)}
      </div>
    </React.Fragment>
  );
}
  
export default ProductGrid;