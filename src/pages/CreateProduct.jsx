import React from 'react';
import {Link} from "react-router-dom";
import { useHistory } from 'react-router';

import productsServices from '../services/productsServices';


/*Componente pendiente de refactoring para aplicar un estilo más declarativo y limpio, 
hecho así para ahorrar tiempo. */

function CreateProduct() {
    const history = useHistory();
    const createProduct = async event => {
        const product = await productsServices.createProduct(event.target.closest('form'));
        if (product.id){
            history.push('/products');
        }
    }
    return (
        <div className="row justify-content-center pt-5">
            <form className='container-fluid border p-2 bg-light'>
                <div className='row justify-content-center'>
                    <div className='col-md-6 col-12'>
                        <label htmlFor="name">
                            Nombre del producto
                        </label>
                        <input type='text' 
                            className='form-control' 
                            placeholder='Nombre del producto' 
                            name='name' 
                            required
                        />
                    </div>
                    <div className='col-md-6 col-12 mt-2 mt-md-0'>
                        <label htmlFor="price">
                            Precio
                        </label>
                        <input type='number'
                            step='0.01' 
                            className='form-control' 
                            placeholder='Precio' 
                            name='price'
                            required
                            />
                    </div>
                    <div className='col-md-6 col-12 mt-2'>
                        <label htmlFor="image">
                            URL de la imagen del producto
                        </label>
                        <input 
                            type='url' 
                            className='form-control' 
                            placeholder='https://example.com' 
                            required
                            name='image' />
                    </div>
                    <div className='w-100 mt-2'></div>
                    <div className='col-6 col-md-3'>
                        <Link to={{
                            pathname: '/products'
                        }} 
                        className='btn btn-light form-control text-primary border'>
                        Cancelar
                        </Link>
                    </div>
                    <div className='col-6 col-md-3'>
                        <button 
                            className='btn btn-dark form-control' 
                            type='button' 
                            onClick={event => createProduct(event)}
                        >
                            Crear
                        </button>
                    </div>            
                </div>
            </form>
        </div>
    );
}

export default CreateProduct;