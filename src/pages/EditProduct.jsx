import React, { useState, useEffect } from 'react';
import {Link} from "react-router-dom";
import { useHistory } from 'react-router';

import productsServices from '../services/productsServices';

/*Componente pendiente de refactoring para aplicar un estilo más declarativo y limpio, 
hecho así para ahorrar tiempo. */

function EditProduct(props) {
    const productId = props.match.params.productId;
    
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState('');

    const history = useHistory();

    const updateProduct = async event => {
        const product = await productsServices.updateProduct(productId, event.target.closest('form'));
        if (product.id){
            history.push('/products');
        }
    }
    
    const setProductInfo = async _ => {
        const product = await productsServices.getProductInfo(productId);
        setName(product.name);
        setPrice(Number(product.price.split('€')[1]));
        setImage(product.image);
    }
    useEffect(() => {
        setProductInfo(productId)
    }, []);
    const deleteProduct = async _ => {
        if (window.confirm('¿Desea borrar este producto?')){
            await productsServices.deleteProduct(productId);
            history.push('/products');
        }
    }

    return (
        <div className="row justify-content-center pt-5">
            <form className='container-fluid border p-2 bg-light'>
                <input type='hidden' value = {image} name = 'image' /> 
                <div className='row justify-content-center'>
                    <div className='col-md-6 col-12'>
                        <label htmlFor="name">
                            Nombre del producto
                        </label>
                        <input type='text' 
                            className='form-control' 
                            placeholder='Nombre del producto' 
                            name='name' 
                            required
                            value = {name}
                            onChange = { event => setName(event.target.value) }
                        />
                    </div>
                    <div className='col-md-6 col-12 mt-2 mt-md-0'>
                        <label htmlFor="price">
                            Precio
                        </label>
                        <input type='number'
                            step='0.01' 
                            className='form-control' 
                            placeholder='Precio' 
                            name='price'
                            required
                            value = {price}
                            onChange = { event => setPrice(event.target.value) }

                        />
                    </div>
                    <div className='w-100 mt-5'></div>
                    <div className='col-12 col-md-3'>
                        <Link to={{
                            pathname: '/products'
                        }} 
                        className='btn btn-light form-control text-primary border'>
                            Cancelar
                        </Link>
                    </div>
                    <div className='col-12 col-md-3'>
                        <button 
                            className='btn btn-danger form-control' 
                            type='button' 
                            onClick={ _ => deleteProduct()}
                        >
                            Borrar
                        </button>
                    </div>
                    <div className='col-12 col-md-3'>
                        <button 
                            className='btn btn-dark form-control' 
                            type='button' 
                            onClick={event => updateProduct(event)}
                        >
                            Actualizar
                        </button>
                    </div>            
                </div>
            </form>
        </div>
    );
}

export default EditProduct;