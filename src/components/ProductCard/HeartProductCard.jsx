import React, { useState } from 'react';
import HeartFilledProductCard from './HeartFilledProductCard';
import HeartTransparentProductCard from './HeartTransparentProductCard';

const style = {
  heartStyle:{
    position:2
  }
}

const isAFavoriteProduct = productId => {
  const favoriteProducts = JSON.parse(localStorage.favoriteProducts);
  return favoriteProducts[productId] ? favoriteProducts[productId] : '0';
}

const setFavoriteProduct = productId => {
  const favoriteProducts = JSON.parse(localStorage.favoriteProducts);
  favoriteProducts[productId] = '1';
  localStorage.setItem('favoriteProducts', JSON.stringify(favoriteProducts))
}

const unsetFavoriteProduct = productId => {
  const favoriteProducts = JSON.parse(localStorage.favoriteProducts);
  favoriteProducts[productId] = '0';
  localStorage.setItem('favoriteProducts', JSON.stringify(favoriteProducts))
}

function HeartProductCard(props) {
  const [isFavorite, changeFavorite] = useState(isAFavoriteProduct(props.productId))
  const mergedStyles = Object.assign(style.heartStyle, isFavorite  === '1' ? style.heartStyleFilled : {})
  
  return (
    <React.Fragment>
      <div style = {mergedStyles}>   
          { isFavorite === '1' ?
            <span 
              onClick={ 
                () => {
                  unsetFavoriteProduct(props.productId);
                  changeFavorite('0');
                }
              }
              className='w-100'
            >
              <HeartFilledProductCard /> 
            </span>
            :
            <span 
              onClick={ 
                () => {
                  setFavoriteProduct(props.productId);
                  changeFavorite('1');
                }
              }
            >
              <HeartTransparentProductCard />
            </span>
          }
      </div>
    </React.Fragment>
  );
}
  
export default HeartProductCard;