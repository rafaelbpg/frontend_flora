import React from 'react';

const style = {
    hidden:{
        textOverflow: "ellipsis", 
        overflow: "hidden", 
        whiteSpace: "nowrap"
    }
}
function NameProductCard(props) {
  return (
    <React.Fragment>
      <div className="h5" style={style.hidden}> {props.name} </div>
    </React.Fragment>
  );
}
  
export default NameProductCard;