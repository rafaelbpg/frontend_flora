import React from 'react';
const HEART_ICON_FILLED = '&#10084;';
const style = {
    heartStyle:{
      color:'red',
      position:'absolute',
      right:'0',
      cursor:'pointer'
    }

}
function HeartFilledProductCard(props) {
  return (
    <React.Fragment>
      <div 
          style = {style.heartStyle}
          dangerouslySetInnerHTML = {{ __html: HEART_ICON_FILLED }}
      > 
      </div>
    </React.Fragment>
  );
}
  
export default HeartFilledProductCard;