import React from 'react';

import ImageProductCard from './ImageProductCard';
import NameProductCard from './NameProductCard';
import PrecioProductCard from './PrecioProductCard';

function ProductCard(props) {

  return (
    <React.Fragment>
      <div className="col-md-4 p-md-3" key = {props.product.id}>
          <ImageProductCard 
            image = {props.product.image } 
            productId = {props.product.id}
            name = {props.product.name}
          />
          <NameProductCard name = {props.product.name } />
          <PrecioProductCard price = {props.product.price  } />
      </div>
    </React.Fragment>
  );
}
    
export default ProductCard;