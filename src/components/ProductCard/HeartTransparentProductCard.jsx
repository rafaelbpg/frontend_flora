import React from 'react';
const HEART_ICON_TRANSPARENT = "&#9825;";
const style = {
  heartStyle:{
    position:'absolute',
    right:'0',
    cursor:'pointer'
  }

}
function HeartTransparentProductCard(props) {
  return (
    <React.Fragment>
      <div 
        style={style.heartStyle}
        dangerouslySetInnerHTML = {{__html: HEART_ICON_TRANSPARENT }}
      > 
      </div>
    </React.Fragment>
  );
}
  
export default HeartTransparentProductCard;