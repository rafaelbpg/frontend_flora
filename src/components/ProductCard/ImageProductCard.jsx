import React from 'react';
import { useHistory } from 'react-router';
import HeartProductCard from './HeartProductCard';
const style={
    relativeBlock : {
      position:'relative',
      fontSize:'1.5rem'
    },
    cursorPointer:{
      cursor:'pointer'
    }
}
function ImageProductCard(props) {
  const history = useHistory();
  return (
    <React.Fragment>
      <div style={style.relativeBlock}>
        <HeartProductCard productId = {props.productId} />
        <img 
          src = {props.image} 
          className = "img img-fluid"
          alt = {props.name} 
          style={style.cursorPointer}
          onClick={ () => history.push(`/products/edit/${props.productId}`)}
        />
      </div>
    </React.Fragment>
  );
}

export default ImageProductCard;