import React from 'react';
import {Link} from "react-router-dom";

function Home() {
  return (
    <div className="row justify-content-center">
      <div className="col-6 text-center mt-5">
        <Link to={{
            pathname: '/products'
          }} 
          className='btn btn-dark'>
          Productos
        </Link>
      </div>
    </div>
    
  );
}

export default Home;