import React from 'react';

import {
  BrowserRouter as Router, Switch, Redirect,
  Route
} from "react-router-dom";

import ProductGrid from './pages/ProductGrid';
import './App.scss';
import CreateProduct from './pages/CreateProduct';
import EditProduct from './pages/EditProduct';

function App() {
  if (typeof localStorage.favoriteProducts == 'undefined'){
    localStorage.setItem('favoriteProducts', '{}');
  }
  
  return (
    <div className="App">
      <div className="container">
        <Router>
          <Switch>
            <Route exact path="/">
              <Redirect to='/products' /> 
            </Route>
            <Route exact path="/products" component={ProductGrid} />
            <Route exact path="/products/create" component={CreateProduct} />
            <Route exact path="/products/edit/:productId" component={EditProduct} />

          </Switch>
        </Router>
      </div>
    </div>
  );
}

export default App;
