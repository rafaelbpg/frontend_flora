# Requisitos para correr la aplicación

Tener npm instalado, en la raíz del proyecto correr en la consola  primero `npm install`  y  luego `npm run start`.

## Tecnología utilizada

La aplicación está hecha con ReactJS com componentes netamente funcionales.

### Problema con la consistencia de los JSON devueltos por la API.

En varias oportunidades me di cuenta que la consistencia de los resultados por
alguna extraña razón cambiaba, un producto por ejemplo podía traer valores como
_name, _image, etc. No he creado una función para saber si un objeto es un producto válido.

